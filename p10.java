//WAP to print those elements whose addition of digits is even.
//eg:- 26 = 2+6 = 8 (even)


import java.io.*;
class P10{
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size of array: ");
		int n = Integer.parseInt(br.readLine());

		int arr1[] = new int[n];

		System.out.println("Enter elements of array: ");
		for(int i=0;i<arr1.length;i++){
			arr1[i] = Integer.parseInt(br.readLine());
			}

		System.out.println("Elements whose addition of digits is even -  ");
		int sum=0;
		for(int i=0;i<arr1.length;i++){
			int temp = arr1[i];
			
			while(temp!=0){
				int rem = temp%10; //last digit
				sum=sum+rem;
				temp = temp/10;
			}

				if(sum %2==0){
					System.out.println(arr1[i]+" ");
				}
				sum=0;
			}
		}
	}


		
	


		
