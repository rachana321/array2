//Find number of even and odd integers in an given array


import java.io.*;
class P2{
	public static void main(String[] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size");
		int n=Integer.parseInt(br.readLine());

		int arr[]=new int[n];
		
		int odd=0,even=0;

		System.out.println("Enter elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());

		       if(arr[i]%2==0){
			       even++;
		}
		else{
			odd++;
		}
		}

		System.out.println("Number of even elements : "+even);
		System.out.println("Number of odd elements : "+odd);
	}
}




