// Find sum of even and odd elements in an array.

import java.io.*;
class P3{
	public static void main(String[] args) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size");
		int n=Integer.parseInt(br.readLine());

		int arr[]=new int[n];
		
		int odd=0,even=0;

		System.out.println("Enter elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());

		       if(arr[i]%2==0){
			       even = arr[i]+even;
		}
		else{
			odd = arr[i]+odd;
		}
		}

		System.out.println("Sum of even elements : "+even);
		System.out.println("Sum of odd elements : "+odd);
	}
}


