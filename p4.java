//WAP to search a specific element from an array and return its index.

import java.io.*;
class P4{
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];

		System.out.println("Enter elements");
		for(int i=0;i<arr.length;i++){
			arr[i] = Integer.parseInt(br.readLine());
		}
		System.out.println("Enter element to search ");
		int x = Integer.parseInt(br.readLine());

		for(int i=0;i<arr.length;i++){
			if(x==arr[i]){
				System.out.println("Element found at index "+i);
			}
		}
	}
}


